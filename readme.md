# Marlin 2.1.2 for the BBT SKR 1.4, TMC2209, BLTouch & Ender 3 Pro.

Please note that this configuration is built for the following hardware:

- BBT SKR 1.4
- TMC2209
- BLTouch (any version should work v3 used in this configuration; review wiring diagram)
- Ender 3 Pro

Visit the blog for more information on this [Custom Marlin Firmware](https://oddity.oddineers.co.uk/2023/04/04/marlin-2-1-2-ender-3-pro-btt-skr-1-4-turbo-abl-bltouch-v3-1/)

## Firmware details 

Advanced Memory Usage is available via "PlatformIO Home > Project Inspect"
RAM:   [=======   ]  71.5% (used 23404 bytes from 32736 bytes)
Flash: [=====     ]  53.0% (used 251888 bytes from 475136 bytes)

## PlatformIO Configuration

The `default_envs` should be set to `LPC1769` in `platform.ini` section `[platformio]`.